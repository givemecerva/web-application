<?php
session_start("login");

/* Testa se a variável de sessão está setada corretamente */
/* Caso contrário, gera erro acusando que o login não foi feito */
if( !isset($_SESSION["nome_usuario_logado"]) )
    header('Location: login/index.php?ERRO=2');


require_once ('model/produto.php');
require_once ('model/token.php');
require_once ('model/usuario.php');

require_once ('db/tblComandas.php');
require_once ('db/tblConfiguracoes.php');
require_once ('db/tblMaquinas.php');
require_once ('db/tblMostrador.php');
require_once ('db/tblProdutos.php');
require_once ('db/tblUsuario.php');


?>



<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Painel do Gerente - Bring me Cerva</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">Bring me Cerva</a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
                
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php echo $_SESSION["nome_usuario_logado"] ?> <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="change_password.php"><i class="fa fa-fw fa-user"></i> Trocar senha</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="logout.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li class="active">
                        <a href="javascript:;" data-toggle="collapse" data-target="#demo"><i class="fa fa-wrench"></i> Configurações</a>
                        <ul id="demo" class="collapse">
                            <li>
                                <a href="?pagina=tara_produtos" target="conteudo">Tara dos produtos</a>
                            </li>
                            <li>
                                <a href="?pagina=ia">Inteligência Artificial</a>
                            </li>
                            <li>
                                <a href="?pagina=estabelecimento">Estabelecimento</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="?pagina=estatistica"><i class="fa fa-fw fa-bar-chart-o"></i> Estatísticas</a>
                    </li>
                    <li>
                        <a href="mostrador/index.php"><i class="fa fa-fw fa-table"></i> Sensores ativos</a>
                    </li>

                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

               <?php
                    switch ($_GET["pagina"]) {
                        case "tara_produtos":
                            include('paginas/tara_produtos.php');
                            break;
                        case "ia":
                            include('paginas/ia.php');
                            break;
                        case "estabelecimento":
                            include('paginas/estabelecimento.php');
                            break;
                        case "estatistica":
                            include('paginas/estatistica.php');
                            break;
                        case "adicionar_token":
                            include('paginas/adicionar_token.php');
                            break;
                        case "remover_token":
                            include('paginas/remover_token.php');
                            break;
                        case "add_sensores":
                            include('paginas/add_sensores.php');
                            break;
                        case "remove_sensores":
                            include('paginas/remove_sensores.php');
                            break;
                        default:
                            include('paginas/painel_principal.php');
                    }
               ?>

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="js/plugins/morris/raphael.min.js"></script>
    <script src="js/plugins/morris/morris.min.js"></script>
    <script src="js/plugins/morris/morris-data.js"></script>

</body>

</html>
