<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <!-- This file has been downloaded from Bootsnipp.com. Enjoy! -->
    <title>Give me Cerva | Login</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="_css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="_css/login.css" />
    <script src="_js/jquery-1.11.1.min.js"></script>
    <script src="_js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="pr-wrap">
                <div class="pass-reset">
                    <form action="_scripts/recuperar.php" method="post">
                        <label>Insira o e-mail o qual está cadastrado</label>
                        <input type="email" name="nEmail" placeholder="Email" />
                        <input type="submit" value="Recuperar" class="pass-reset-submit btn btn-success btn-sm" />
                    </form>
                </div>
            </div>
            <div class="wrap">
                <p class="form-title">
                    Login</p>
                <form class="login" action="_scripts/login.php" method="post">
                    <input type="text" placeholder="Usuário" name="nLogin" />
                    <input type="password" placeholder="Senha" name="nSenha" />
                    <input type="submit" value="Entrar" class="btn btn-success btn-sm" />
                    <?php
                        if( isset($_GET["ERRO"]) && $_GET["ERRO"] == 1 ){ ?>
                            <br /><br />
                            <center>
                                <span class="erromsg">Usuário e/ou senha incorretos!</span>
                            </center>
                    <?php }
                        elseif( isset($_GET["ERRO"]) && $_GET["ERRO"] == 2 ){ ?>
                            <br /><br />
                            <center>
                                <span class="erromsg">Efetue o login!</span>
                            </center>
                    <?php } ?>

                    <div class="remember-forgot">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" />
                                        Lembre-me
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-6 forgot-pass-content">
                                <a href="javascript:void(0)" class="forgot-pass">Esqueci a Senha</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
 $(document).ready(function () {
    $('.forgot-pass').click(function(event) {
      $(".pr-wrap").toggleClass("show-pass-reset");
    }); 
    
    $('.pass-reset-submit').click(function(event) {
      $(".pr-wrap").removeClass("show-pass-reset");
    }); 
});
</script>
</body>
</html>