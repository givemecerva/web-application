<?php
/* Inicia nova sessão de navegação onde é possível setar variáveis de seção */
session_start("login");
/* Configura diretivas de processamento p/ mostrar e reportar erros do código*/
ini_set('error_reporting', E_ALL|E_STRICT);
ini_set('display_errors', 1);


/* Inclui classe tblUsuario */
require_once dirname(dirname(dirname(__FILE__))) . '/db/tblUsuario.php';


/* Pega variáveis que vieram de index.php no método POST */
$login = strtolower($_POST["nLogin"]);
$senha = $_POST["nSenha"];

/* Manda buscar os dados do usuário com login igual ao recebido de index.php*/
/* Se não encontrar nada no banco de dados, retorna false */
//$usuario = new Usuario();
$usuario = TblUsuario::recuperarPorLogin($login);



/* Testa se encontrou algum usuário cadastrado com aquele login */
if($usuario){
	/* Criptografa a senha recebida de index.php pra poder comparar */
	$senhaCriptografada = md5($senha);

	/* Agora testa se a senha recebida é igual à que consta do banco de dados*/
	if($senhaCriptografada == $usuario->senha){
		/* Seta variáveis de sessão com informações do usuario logado*/
		$_SESSION['id_usuario_logado'] = $usuario->id;
		$_SESSION['nome_usuario_logado'] = $usuario->nome;
		$_SESSION['login_usuario_logado'] = $usuario->login;
		$_SESSION['email_usuario_logado'] = $usuario->email;

		header('Location: ../../index.php');
	}
	/* Caso a senha esteja incorreta gera ERRO 1*/
	else
		header('Location: ../index.php?ERRO=1');

}
/* Caso não tenha encontrado nenhum e-mail válido no banco de dados gera ERRO 1 */
else
	header('Location: ../index.php?ERRO=1')

?>