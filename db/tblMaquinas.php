<?php
    require_once("banco.php");
	require_once("tblComandas.php");
	require_once dirname(dirname(__FILE__)) . '/model/token.php';
	require_once dirname(dirname(__FILE__)) . '/model/mesa.php';

	class TblMaquinas{
		public static function recuperar($mac, $valor){
			$bd = new Banco();

			$query_select = "SELECT maquinas.id, maquinas.mac, maquinas.mesa, maquinas.fk_produto_id, maquinas.coeff_a, maquinas.coeff_b,
							produtos.nome, produtos.vazio, produtos.cheio, produtos.logo, produtos.limiar_alerta, 
							comandas.abertura 
							FROM produtos 
							INNER JOIN maquinas 
							ON maquinas.fk_produto_id=produtos.id 
							INNER JOIN comandas
							ON maquinas.mesa=comandas.mesa 
							WHERE maquinas.mac='$mac'";

			$resultado = $bd -> select($query_select);


			if (count($resultado) === 0){
				return null;
			}else{
				$tokenTmp = new Token();

				$tokenTmp->mac_address = $mac;
				$tokenTmp->valor = $valor;
				$tokenTmp->mesa = $resultado[0]["mesa"];
				$tokenTmp->id = $resultado[0]["id"];
				$tokenTmp->logo = $resultado[0]["logo"];
				$tokenTmp->produto = $resultado[0]["fk_produto_id"];
				$tokenTmp->calib_a = $resultado[0]["coeff_a"];
				$tokenTmp->calib_b = $resultado[0]["coeff_b"];

				$massa_total = $resultado[0]["cheio"] - $resultado[0]["vazio"];

				$weightInGrams = $tokenTmp->getWeightInGrams($valor);
				$tokenTmp->porcentagem = (($weightInGrams - $resultado[0]["vazio"]) / $massa_total)*100;

				if ($tokenTmp->porcentagem <= $resultado[0]["limiar_alerta"]){
					$tokenTmp->isVazio = true;
				}else{
					$tokenTmp->isVazio = false;
				}

				$tokenTmp->inicio = $resultado[0]["abertura"];
				return $tokenTmp;
			}
		}

		public static function pertenceAoEstabelecimento($mac)
		{
			$bd = new Banco();

			$query = "SELECT * FROM maquinas WHERE mac='$mac'";

			$resultado = $bd -> query($query);

			if (mysqli_num_rows($resultado) === 0){
				return false;
			}else{
				return true;
			}
		}

		public static function devolverTokenPorMac($token)
		{
			$bd = new Banco();

			$query = "UPDATE maquinas 
					  SET mesa='-1' 
					  WHERE mac='$token->mac_address'";

			$resultado = $bd -> query($query);

			if ($resultado === false){
				return false;
			}else{
				return true;
			}
		}

		public static function tokensNaMesa($mesa)
		{
			$bd = new Banco();

			$query = "SELECT * FROM maquinas WHERE mesa='$mesa'";
			$resultado = $bd -> query($query);

			return mysqli_num_rows($resultado);
		}
		
		public static function retornaIDdosTokensDisponiveis()
		{
			$bd = new Banco();
			
			$query = "SELECT id FROM `maquinas` WHERE mesa='-1'";
			$resultado = $bd -> select($query);
			
			$id = array();
			for($i = 0; $i< count($resultado); $i++)
			{
				$id[]= $resultado[$i]["id"];
				//echo "<option value=\"" . $resultado[$i]["id"] . "\">" . $resultado[$i]["id"] .	"</option>\n";
			}		
			return $id;
		}
		
		public static function getQuantidadeTokensDisponiveis()
		{
			$bd = new Banco();
			
			$query = "SELECT * FROM maquinas WHERE mesa = '-1'";
			$resultado = $bd -> query($query);
			
			return mysqli_num_rows($resultado);
		}
		
		public static function atualizaMaquina($mesa)
		{
			$bd = new Banco();
			
			$query = "UPDATE maquinas SET mesa = '$mesa->nro_mesa' , fk_produto_id = '$mesa->produto' WHERE id = '$mesa->id_maquina'";
			
			return $bd -> query($query);
		}
		
		
		public static function devolverTokenPorId($token_id)
		{
			$bd = new Banco();

			$query = "UPDATE maquinas 
					  SET mesa='-1' 
					  WHERE id='$token_id'";

			$resultado = $bd -> query($query);

			if ($resultado === false){
				return false;
			}else{
				return true;
			}
		}

		public static function verificaRemocao($token_id)
		{
			$bd = new Banco();
			
			$query = "SELECT mesa FROM maquinas WHERE id = '$token_id'";
			$resultado = $bd->select($query);
			
			if($resultado[0]["mesa"] == -1)
			{
				return true;
			}else{
				return false;
			}
		}
		
		public static function getMesaPorId($token_id)
		{
			$bd = new Banco();
			$query = "SELECT * FROM maquinas WHERE id = '$token_id'";
			$resultado = $bd->select($query);
			
			$mesa = new Mesa();
			
			$mesa->nro_mesa = $resultado[0]["mesa"];
			$mesa->produto = $resultado[0]["fk_produto_id"];
			$mesa->id_maquina = $resultado[0]["id"];
			
			return $mesa;
			
		}
	}
	
?>
