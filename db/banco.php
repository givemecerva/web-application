<?php

class Banco{
	protected static $connection;

	public static function connect(){
		if(!isset(self::$connection)) {

			// Carrega dados de login
			$config = parse_ini_file('login.ini');

			// Efetua a conexao statica usando os dados do login.ini
			self::$connection = new mysqli($config['ip_addr'],
										 $config['username'],
										 $config['password'],
										 $config['dbname']);
		}

		if (self::$connection === false){
			return false;
		}

		return self::$connection;
	}

	public function query($query) {
        $connection = $this -> connect();

        // Query the database
        $result = $connection -> query($query);

        return $result;
    }

	public function select($query){
		$rows = array();
		$result = $this -> query($query);

		if ($result === false){
			return false;
		}

		while ($row = $result -> fetch_assoc()) {
			$rows[] = $row;
		}

		return $rows;
	}

	public function error() {
        $connection = $this -> connect();
        return $connection -> error;
    }

	public function quote($value) {
        $connection = $this -> connect();
        return "'" . $connection -> real_escape_string($value) . "'";
    }

    public function getTabela($nome_da_tabela){
    	$query = "SELECT * FROM $nome_da_tabela";
    	$resultado = $this->select($query);

    	return $resultado;
    }

    public function getNroLinhas($tabela){
    	return count($tabela);
    }
}

?>