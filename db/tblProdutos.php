<?php

	require_once("banco.php");
	require_once dirname(dirname(__FILE__)) . '/model/produto.php';


	class TblProdutos{
		public static function recuperarPorID($id)
		{
			$bd = new Banco();

			$query_select = "SELECT * FROM produtos WHERE id=$id";
			$resultado = $bd -> select($query_select);

			if ($resultado === false)
			{
				return false;
			}else
			{
				// Cria produto para retorno
				$prod = new Produto();

				$prod->id = $resultado[0]["id"];
				$prod->nome = $resultado[0]["nome"];
				$prod->pesoVazio = $resultado[0]["vazio"];
				$prod->pesoCheio = $resultado[0]["cheio"];
				$prod->logo = $resultado[0]["logo"];
				$prod->limiarAlerta= $resultado[0]["limiar_alerta"];
			}

			return $prod;
		}

		public static function recuperarPorCampo($campo, $valor)
		{
			$bd = new Banco();

			$query_select = "SELECT * FROM produtos WHERE $campo='$valor'";
			$resultado = $bd -> select($query_select);

			if ($resultado === false)
			{
				return false;
			}else
			{
				// Cria produto para retorno
				$prod = new Produto();

				$prod->id = $resultado[0]["id"];
				$prod->nome = $resultado[0]["nome"];
				$prod->pesoVazio = $resultado[0]["vazio"];
				$prod->pesoCheio = $resultado[0]["cheio"];
				$prod->logo = $resultado[0]["logo"];
				$prod->limiarAlerta= $resultado[0]["limiar_alerta"];
			}

			return $prod;
		}

		public static function recuperarTodos()
		{
			$bd = new Banco();

			$query_select = "SELECT * FROM produtos";

			$resultado = $bd -> select($query_select);

			if ($resultado === false)
			{
				echo "Nenhum produto cadastrado...";
				return null;
			}else
			{
				$produtos = new SplFixedArray(count($resultado));
				// Itera pelas linhas da tabela
				for ($i = 0; $i < count($resultado); $i++) {

    				// Cria produto para retorno
					$prod = new Produto();

					$prod->id = $resultado[$i]["id"];
					$prod->nome = $resultado[$i]["nome"];
					$prod->pesoVazio = $resultado[$i]["vazio"];
					$prod->pesoCheio = $resultado[$i]["cheio"];
					$prod->logo = $resultado[$i]["logo"];
					$prod->limiarAlerta= $resultado[$i]["limiar_alerta"];

					$produtos[$i] = $prod;
				}
				
			}

			return $produtos;
		}

		public static function inserir($produto)
		{
			$bd = new Banco();

			$query = "INSERT INTO produtos (nome, vazio, cheio, logo, limiar_alerta)
					  VALUES('$produto->nome', $produto->pesoVazio, $produto->pesoCheio, '$produto->logo', $produto->limiarAlerta)";

			return $bd->query($query);
		}

		public static function atualizar($produto)
		{
			$bd = new Banco();

			$query = "UPDATE produtos 
					  SET nome='$produto->nome', vazio='$produto->pesoVazio', cheio='$produto->pesoCheio', logo='$produto->logo', limiar_alerta='$produto->limiarAlerta'
					  WHERE id='$produto->id'";

			return $bd->query($query);	
		}

		public static function excluir($produto)
		{
			$bd = new Banco();

			$query = "DELETE FROM produtos WHERE id='$produto->id'";

			return $bd->query($query);
		}

		public static function excluirPorID($id)
		{
			$bd = new Banco();
			$id = $bd->quote($id);

			$query = "DELETE FROM produtos WHERE id=$id";

			return $bd->query($query);
		}

		public static function getQuantidadeProdutos()
		{
			
			$bd = new Banco();

			$query_select = "SELECT * FROM produtos";

			$resultado = $bd -> select($query_select);

			if ($resultado === false)
			{
				echo "Nenhum produto cadastrado...";
				return null;
			}
			
			return (count($resultado));	
			
			
		}
			
	}
	
?>