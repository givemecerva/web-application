<?php
    require_once("banco.php");

	class TblConfiguracoes{
		protected static function set($parametro, $valor)
		{
			$bd = new Banco();

			$parametro = $bd->quote($parametro);
			$valor = $bd->quote($valor);
			$query = "UPDATE configuracoes SET valor=$valor  WHERE nome=$parametro";

			return $bd->query($query);	
		}

		protected static function get($parametro)
		{
			$bd = new Banco();

			$parametro = $bd->quote($parametro);
			$query = "SELECT valor FROM configuracoes WHERE nome=$parametro";

			$resultado = $bd->select($query);	

			if ($resultado === false)
			{
				return false;
			}else
			{
				return $resultado[0]["valor"];
			}
		}

		public static function setLogo($logo)
		{
			tblConfiguracoes::set('estabelecimento_logo', $logo);	
		}

		public static function getLogo()
		{
			return tblConfiguracoes::get('estabelecimento_logo');
		}

		public static function setNomeEstabelecimento($nome)
		{
			tblConfiguracoes::set('estabelecimento_nome', $nome);
		}

		public static function getNomeEstabelecimento()
		{
			return tblConfiguracoes::get('estabelecimento_nome');
		}

		public static function setAlgoritmoIA($nro)
		{
			tblConfiguracoes::set('ia', $nro);
		}

		public static function getAlgoritmoIA()
		{
			return tblConfiguracoes::get('ia');
		}
	}
	
?>