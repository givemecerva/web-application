<?php
	require_once("tblComandas.php");
	require_once("tblMaquinas.php");
	require_once dirname(dirname(__FILE__)) . '/model/token.php';

	$mac = "aa:bb:cc:dd:ee:ff";

	if (tblMaquinas::pertenceAoEstabelecimento($mac))
	{
		echo "O mac $mac pertence ao estabelecimento...";
		$tokenTmp = tblMaquinas::recuperar($mac,720);

		if ($tokenTmp === null)
		{
			echo "O token não está em uso";
		}else
		{
			$tokenTmp->exibir();
			// if (tblMaquinas::devolverToken($tokenTmp) === true)
			// {
			// 	echo "<br><br> Token devolvido com sucesso... ";

			// 	// Se não houver mais tokens na mesa, fecha a conta
			// 	if (tblMaquinas::tokensNaMesa($tokenTmp->mesa) == 0)
			// 	{
			// 		tblComandas::fecharComanda($tokenTmp->mesa);
			// 		echo "<br><br> Como era o ultimo token da mesa, fechei a comanda... ";
			// 	}
			// }else
			// {
			// 	echo "<br><br> O token não pode ser devolvido com sucesso... ";
			// }
		};

	}else
	{
		echo "O mac $mac não pertence ao estabelecimento...";
	}
?>