<?php
	require_once("tblProdutos.php");
	require_once dirname(dirname(__FILE__)) . '/model/produto.php';

	// Cria produto
	$prod = new Produto();
	
	$prod->nome = "Stela Artois";
	$prod->pesoVazio = 350;
	$prod->pesoCheio = 1020;
	$prod->logo = "stela.jpg";
	$prod->limiarAlerta = 25;

	//$prod->exibir();

	// Insert
	//tblProdutos::inserir($prod);

	// Select by ID
	//$kaiser = tblProdutos::fromId(2);
	//$kaiser->exibir();

	// Select by Campo
	//$bud = tblProdutos::recuperarPorCampo("nome", "Buddweiser");
	//$bud->exibir();

	// Update - Muda o nome da kaiser para nova schin
	//$kaiser = tblProdutos::fromID(2);
	//$kaiser->nome = "Nova Schin";
	//tblProdutos::atualizar($kaiser);

	// Excluir - Ativar o Insert, depois o excluir
	//$stela = tblProdutos::fromID(6);
	//tblProdutos::excluir($stela);

	//$produtos = tblProdutos::recuperarTodos();
	//$produtos[1]->exibir();
?>