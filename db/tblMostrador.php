<?php
    require_once("banco.php");
    require_once("tblMaquinas.php");
    require_once dirname(dirname(__FILE__)) . '/model/token.php';
    require_once("tblConfiguracoes.php");


	class TblMostrador{
		public static function inserirToken($token)
		{
			$bd = new Banco();

			$data_e_hora = date('Y-m-d H:i:s');
			
			$query = "INSERT INTO mostrador (mac, valor) 
					  VALUES('$token->mac_address', '$token->valor')";

			return $bd->query($query);
		}

		public static function excluirToken($token)
		{
			$bd = new Banco();

			$query = "DELETE FROM mostrador WHERE mac='$token->mac_address'";

			return $bd->query($query);
		}

		public static function atualizarToken($token)
		{
			$bd = new Banco();

			$query = "UPDATE mostrador SET valor = '$token->valor', porcentagem = '$token->porcentagem' WHERE mac='$token->mac_address'";

			return $bd->query($query);
		}

		public static function existe($mac)
		{
			$bd = new Banco();

			$query_select = "SELECT * FROM mostrador WHERE mac='$mac'";

			$resultado = $bd ->query($query_select);

			if (mysqli_num_rows($resultado) === 0){
				return false;
			}else{
				return true;
			}
		}

		public static function exibirLista()
		{
			$bd = new Banco();

			/*
			if (tblConfiguracoes::getAlgoritmoIA() == "1")
			{
				$query_select = "SELECT mac, valor FROM mostrador ORDER BY valor";
			}
			else if (tblConfiguracoes::getAlgoritmoIA() == "2")
			{
				$query_select = "SELECT mac, valor FROM mostrador ORDER BY inicio DESC";
			}else if (tblConfiguracoes::getAlgoritmoIA() == "3")
			{
				$query_select = "SELECT mac, valor FROM mostrador ORDER BY inicio ASC";
			}else
			{
				return;
			}
			*/

			$query_select = "SELECT * FROM mostrador";
			$resultado = $bd -> select($query_select);

			if ($resultado === false)
			{
				echo "Vazio...";
				return null;
			}else
			{
				for ($i = 0; $i < count($resultado); $i++) {
						$tokenTmp = tblMaquinas::recuperar($resultado[$i]["mac"],$resultado[$i]["valor"]);

						$tokenTmp->exibir();
				}
			}
		}
	}
	
?>
