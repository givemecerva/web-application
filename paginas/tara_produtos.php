<?php

    if( (isset($_POST["txt_nome"])) AND
        (isset($_POST["num_vazio"])) AND
        (isset($_POST["num_cheio"])) AND
        (isset($_POST["logo"])) AND
        (isset($_POST["sel_limiar"])) )
    {
        $produto = new Produto();
        $produto->nome = $_POST["txt_nome"];
        $produto->pesoVazio = $_POST["num_vazio"];
        $produto->pesoCheio = $_POST["num_cheio"];
        $produto->logo = $_POST["logo"];
        $produto->limiarAlerta = $_POST["sel_limiar"];  


        //echo "caralho:";
        //var_dump($produto);


        tblProdutos::inserir($produto);
    }

    elseif( isset($_GET["deletaProd"]) )
        $resultado = tblProdutos::excluirPorID($_GET["deletaProd"]);

?>




<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Produtos <small>Definição dos pesos</small>
        </h1>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="alert alert-info alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <i class="fa fa-info-circle"></i>  <strong>Possui alguma dúvida?</strong> Entre em contato <a href="http://www.hexcode.com.br" class="alert-link">conosco</a>!
        </div>
    </div>
</div>

<div class="row">

        <h2>Produtos Cadastrados</h2>
        <div class="table-responsive">
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th><center>ID</center></th>
                        <th><center>Nome</center></th>
                        <th><center>Peso Vazio [gramas]</center></th>
                        <th><center>Peso Cheio [gramas]</center></th>
                        <th><center>Logo</center></th>
                        <th><center>Porcentagem de Alerta</center></th>
                        <th><center>Ação</center></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        // Lista todos os produtos do banco
                        $produtos = tblProdutos::recuperarTodos();
                        if ($produtos != null)
                        {
                            for($i = 0; $i < count($produtos); $i++)
                            {
                                $produtos[$i]->exibir();
                            }
                        }
                    ?>
                </tbody>
            </table>
        </div>
</div>


<div class="row">
    <h2>Inserir novo produto:</h2>

     <div class="col-lg-6">
        <form role="form" action = "?pagina=tara_produtos" method="POST">
            <div class="form-group">
                <label for="txt_nome">Nome:</label>
                <input class="form-control" name="txt_nome" required>
            </div>

            <div class="form-group">
                <label for="num_vazio">Peso Vazio (em gramas):</label><br/>
                <input type="number" class="form-control" name="num_vazio" min="0" max="5000" required>
            </div>

            <div class="form-group">
                <label for="num_cheio">Peso Cheio (em gramas):</label><br/>
                <input type="number" class="form-control" name="num_cheio" min="0" max="10000" required>
            </div>

            <div class="form-group">
                <label for="logo">Logo:</label>
                <input type="text" class="form-control" name="logo" required>
            </div>

            <div class="form-group">
                <label for="sel_limiar">Limiar Alerta:</label><br/>
                <select class="form-control" name="sel_limiar">
                    <option value="50">50%</option>
                    <option value="40">40%</option>
                    <option value="30">30%</option>
                    <option value="25">25%</option>
                    <option value="20">20%</option>
                    <option value="15">15%</option>
                    <option value="10">10%</option>
                    <option value="5">5%</option>
                </select>
            </div>

            <button type="submit" class="btn btn-default">Salvar</button>
            <button type="reset" class="btn btn-default">Limpar</button>
        </form>
    </div>
</div>