<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Bem-Vindo! <small>Página Inicial</small>
        </h1>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="alert alert-info alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <i class="fa fa-info-circle"></i>  <strong>Possui alguma dúvida?</strong> Entre em contato <a href="http://www.hexcode.com.br" class="alert-link">conosco</a>!
        </div>
    </div>
</div>

<div class="row">

    <a href="?pagina=add_sensores">
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-green">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-plus fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge">26</div>
                            <div>Máquinas ativas!</div>
                        </div>
                    </div>
                </div>
                    <div class="panel-footer">
                        <span class="pull-left">Adicionar</span>
                        <span class="pull-right">
                            <i class="fa fa-arrow-circle-right"></i>
                        </span>
                        <div class="clearfix"></div>
                    </div>
               
            </div>
        </div>
    </a>

    <a href="?pagina=remove_sensores">
    <div class="col-lg-3 col-md-6">
        <div class="panel panel-red">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-minus fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge">26</div>
                        <div>Máquinas ativas!</div>
                    </div>
                </div>
            </div>
                <div class="panel-footer">
                    <span class="pull-left">Remover</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
        </div>
    </div>
    </a>

</div>