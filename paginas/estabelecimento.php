<?php

    if( isset($_POST["txt_estabelecimento"]) )
        tblConfiguracoes::setNomeEstabelecimento($_POST["txt_estabelecimento"]);

?>


<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Estabelecimento <small>Informações</small>
        </h1>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="alert alert-info alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <i class="fa fa-info-circle"></i>  <strong>Possui alguma dúvida?</strong> Entre em contato <a href="http://www.hexcode.com.br" class="alert-link">conosco</a>!
        </div>
    </div>
</div>

<div class="row">
     <div class="col-lg-6">
        <form role="form" action="?pagina=estabelecimento" method="POST">
            <div class="form-group">
                <label for="txt_estabelecimento">Nome:</label>
                <input class="form-control" name="txt_estabelecimento" value="<?php echo tblConfiguracoes::getNomeEstabelecimento() ?>" required>
            </div>

            <div class="form-group">
                <label for="input_file">Logotipo:</label>
                <input name="input_file" type="file">
            </div>
            <br/>
            <button type="submit" class="btn btn-default">Salvar</button>
        </form>
    </div>
</div>