<?php

    $isChecked_vazios = false;
    $isChecked_recemchegados = false;
    $isChecked_bebedores = false;

    // Verifica se houve requisicao para mudar de algoritmo
    $escolhaUsuario = $_POST["algoritmo"];

    if ($escolhaUsuario)
        tblConfiguracoes::setAlgoritmoIA($escolhaUsuario);
    
    // Recupera o algoritmo selecionado do banco
    $marcado = tblConfiguracoes::getAlgoritmoIA();

    // Marca como checked, aquele que tiver definido no banco
    if ($marcado === "1")
        $isChecked_vazios = true;

    else if ($marcado === "2")
        $isChecked_recemchegados = true;

    else if ($marcado === "3")
        $isChecked_bebedores = true;

?>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Inteligência Artificial <small>Estratégias de atendimento</small>
        </h1>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="alert alert-info alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <i class="fa fa-info-circle"></i>  <strong>Possui alguma dúvida?</strong> Entre em contato <a href="http://www.hexcode.com.br" class="alert-link">conosco</a>!
        </div>
    </div>
</div>

<form action="?pagina=ia" method="post" name="formAlgoritmo">
 <div class="row">
        <div class="list-group">
                    <div class="radio">
                        <label>
                            <input type="radio" name="algoritmo" id="rdb_vazios" value="1" onclick="document.formAlgoritmo.submit()" 
                            <?php if($isChecked_vazios){ ?> checked <?php } ?> >
                        
                            <h4 class="list-group-item-heading">Priorizar recipientes vazios</h4>
                            <p class="list-group-item-text">Prioriza as mesas onde a cerveja está mais próxima de acabar.</p>
                        </label>
                    </div>
                    <br/>

                    <div class="radio">
                        <label>
                            <input type="radio" name="algoritmo" id="rdb_recemchegados" value="2" onclick="document.formAlgoritmo.submit()"
                            <?php if($isChecked_recemchegados){ ?> checked <?php } ?> >

                            <h4 class="list-group-item-heading">Priorizar recém-chegados</h4>
                            <p class="list-group-item-text">As mesas que acabaram de ser preenchidas terão maior prioridade.</p>
                        </label>
                    </div>
                    <br/>
                    
                    <div class="radio">
                        <label>
                            <input type="radio" name="algoritmo" id="rdb_bebedores" value="3" onclick="document.formAlgoritmo.submit()"
                            <?php if($isChecked_bebedores){ ?> checked <?php } ?> >

                            <h4 class="list-group-item-heading">Priorizar mesas que mais consomem</h4>
                            <p class="list-group-item-text">As mesas que estão tendo uma alta taxa de consumo terão maior prioridade.</p>
                        </label>
                    </div>
                    <br/>
        </div>
</div>
</form>