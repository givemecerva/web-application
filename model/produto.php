<?php
require_once dirname(dirname(__FILE__)) . '/db/banco.php';

class Produto{
	public $id;
	public $nome;
	public $pesoVazio;
	public $pesoCheio;
	public $logo;
	public $limiarAlerta;

	public function exibir(){
		printf("<tr>");
        printf("<td><center> %s </center></td>", $this->id);
        printf("<td><center> %s </center></td>", $this->nome);
        printf("<td><center> %s </center></td>", $this->pesoVazio);
        printf("<td><center> %s </center></td>", $this->pesoCheio);
        printf("<td><center> %s </center></td>", $this->logo);
        printf("<td><center> %s %%</center></td>", $this->limiarAlerta);
        printf("<th><center><a href='?pagina=tara_produtos&deletaProd=%s'><span class=\"label label-danger\">Excluir</span></center></th>", $this->id);
        printf("</tr>");
	}

	public static function exibirTabela()
	{

		$conexao = new Banco();
		$resultados = $conexao->getTabela("produtos");

		$i=0;
		while ($i < $conexao->getNroLinhas($resultados)) 
		{
			$prodTmp = new Produto();
			$prodTmp->fromLinha($resultados[$i]);
			$prodTmp->exibir();

			$i++;
		}
	}

}
?>