<?php
require_once dirname(dirname(__FILE__)) . '/db/banco.php';


class Token{
	public $id;
	public $mac_address;
	public $valor;
	public $mesa;
	public $produto;
	public $logo;
	public $porcentagem;
	public $inicio;
	public $isVazio;
	public $calib_a;
	public $calib_b;

	public function getWeightInGrams($value)
	{
		return (($this->calib_a * $value) + $this->calib_b);
	}

	public function exibir(){
		$logo_completo = "../img/img_produtos/" . $this->logo;

	    printf("<div class=\"panel panel-default\" style=\"width: 200px; height: 120px; float: left; margin: 10px\">");
	    printf("<div class=\"panel-heading\" style=\"background-image: url('%s'); height: 40px\"></div>", $logo_completo);
	    printf("<div class=\"panel-body\">");  
	    printf("<div class=\"progress\">");

	    if ($this->porcentagem < 50)
	    {
	    	printf("<div class=\"progress-bar progress-bar-striped active progress-bar-danger\" role=\"progressbar\" aria-valuenow=\"10\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: %d%%;\">", $this->porcentagem);
		}else{
			printf("<div class=\"progress-bar progress-bar-striped active progress-bar-success\" role=\"progressbar\" aria-valuenow=\"10\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: %d%%;\">", $this->porcentagem);
		}
	   	//printf("<span>%d%%</span>", $this->porcentagem);
	    printf("</div></div><center><h4><b>Mesa %s</b></h4></center>", $this->mesa);
	    printf("</div></div>");
	}
}
?>