﻿<?php
	require_once("db/banco.php");
	require_once ("db/tblMaquinas.php");
	require_once ("db/tblProdutos.php");
	require_once ("model/produto.php");
	require_once ("model/token.php");
	require_once ("model/mesa.php");
	
	$nro_mesa = $_POST["mesa"];
	$produto = $_POST["produto"];
	$id_maquina = $_POST["maquina"];
	
	$mesa  = new Mesa();
	
	$mesa -> nro_mesa = $nro_mesa;
	$mesa -> produto = $produto;
	$mesa -> id_maquina = $id_maquina;
	
	tblMaquinas::atualizaMaquina($mesa);
	
	header('Location: index.php?pagina=add_sensores');
	
	//echo "Produto : " . $mesa->produto . "<br>Mesa: " . $mesa -> nro_mesa. "<br>Maquina:" . $mesa -> id_maquina;
	
?>