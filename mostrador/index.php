<?php
    require_once dirname(dirname(__FILE__)) . '/db/tblConfiguracoes.php';
    require_once dirname(dirname(__FILE__)) . '/db/tblMaquinas.php';
    require_once dirname(dirname(__FILE__)) . '/db/tblMostrador.php';
    require_once dirname(dirname(__FILE__)) . '/model/token.php';

    $estabelecimento = tblConfiguracoes::getNomeEstabelecimento();

    if ($estabelecimento === null)
    {
      $estabelecimento = "Bring me Cerva";
    }
?>

<!DOCTYPE html>
<!-- meta http-equiv="refresh" content="1" -->
<html class="full" lang="pt-br">
<!-- Make sure the <html> tag is set to the .full CSS class. Change the background image in the full.css file. -->

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Mostrador - Give me Cerva</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/full.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script>
    <!-- Funcao em javascript responsavel por fazer a atualizacao assincrona da pagina -->
    function updateAssincrono(){
        var xmlhttp;
        if (window.XMLHttpRequest) {
            xmlhttp=new XMLHttpRequest();
        }

        xmlhttp.onreadystatechange=function(){
            if (xmlhttp.readyState==4 && xmlhttp.status==200)
            {
                document.getElementById("div_assincrona").innerHTML=xmlhttp.responseText;
            }
        }
        
        xmlhttp.open("GET","mostrador_engine.php",true);
        xmlhttp.send();
    }

    <!-- Timeout para chamar a funcao de atualizacao -->
    setInterval(function(){ 
        updateAssincrono();
    }, 100);
    </script>

</head>

<body>

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="../index.php"><?php echo $estabelecimento ?></a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="#">Sobre</a>
                    </li>
                    <li>
                        <a href="#">Serviço</a>
                    </li>
                    <li>
                        <a href="#">Contatos</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- Put your page content here! -->

    <div id="div_assincrona">
        <!-- O conteudo assincrono carrega aqui ;) -->
    </div>

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>
