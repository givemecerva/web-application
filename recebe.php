<?php
	require_once("db/tblMostrador.php");
	require_once("db/tblComandas.php");
	require_once("db/tblMaquinas.php");
	require_once("db/tblProdutos.php");
	require_once("model/token.php");

	$valor = $_GET["valor"];
	$mac = $_GET["mac"];

	if (tblMaquinas::pertenceAoEstabelecimento($mac))
	{
		//echo "O mac $mac pertence ao estabelecimento...";
		$tokenTmp = tblMaquinas::recuperar($mac,$valor);

		if ($tokenTmp === null)
		{
			//echo "O token não está em uso";
		}else
		{
			//printf("Produto consumido: %d", $tokenTmp->produto);
			$produtoTmp = tblProdutos::recuperarPorID($tokenTmp->produto);

			// Retorna a maquina os dados do produto, para nao precisar ficar enviando todo o tempo]
			$limiarAlerta =intval(($produtoTmp->pesoCheio - $produtoTmp->pesoVazio)*($produtoTmp->limiarAlerta/100) + $produtoTmp->pesoVazio);
			echo "$limiarAlerta\r";


			// Se ele está no mostrador
			if (tblMostrador::existe($mac))
			{

				if ($tokenTmp->porcentagem <= $produtoTmp->limiarAlerta)
				{
					tblMostrador::atualizarToken($tokenTmp);
				}else{
					tblMostrador::excluirToken($tokenTmp);
				}
			}else
			{
				//printf("O mac address %s não existe no mostrador", $mac);

				if ($tokenTmp->porcentagem <= $produtoTmp->limiarAlerta)
				{
					tblMostrador::inserirToken($tokenTmp);
				}
			}
			//$tokenTmp->exibir();

			// if (tblMaquinas::devolverToken($tokenTmp) === true)
			// {
			// 	echo "<br><br> Token devolvido com sucesso... ";

			// 	// Se não houver mais tokens na mesa, fecha a conta
			// 	if (tblMaquinas::tokensNaMesa($tokenTmp->mesa) == 0)
			// 	{
			// 		tblComandas::fecharComanda($tokenTmp->mesa);
			// 		echo "<br><br> Como era o ultimo token da mesa, fechei a comanda... ";
			// 	}
			// }else
			// {
			// 	echo "<br><br> O token não pode ser devolvido com sucesso... ";
			// }
		};

	}else
	{
		//echo "O mac $mac não pertence ao estabelecimento...";
	}
?>